<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "token".
 *
 * @property integer $id
 * @property string $name
 * @property string $secret
 * @property string $created_at
 */
class Token extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'secret'], 'required'],
            [['created_at'], 'safe'],
            [['name', 'secret'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['secret'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'secret' => 'Secret',
            'created_at' => 'Created At',
        ];
    }
}
