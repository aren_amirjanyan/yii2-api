<?php
namespace app\modules\api\v1\components;

use Firebase\JWT\JWT;
use Yii;
use yii\base\Component;
use app\modules\api\v1\models\Token;

class ApiComponent extends Component
{
    public function check()
    {
        $headers = Yii::$app->request->headers;
        $x_api_token = $headers->get('x-api-token');
        $token = JWT::decode($x_api_token, Yii::$app->params['secretKey'],['HS256']);
        if($token && Token::findOne(['app_id' => $token->jti])){
            return true;
        }else{
            return false;
        }
    }
}