<?php
namespace app\modules\api\v1\components;

use Yii;
use yii\base\Component;
use app\models\User;

class AuthComponent extends Component
{
    public function check()
    {
        $headers = Yii::$app->request->headers;
        $token = $headers->get('x-authorization-token');
        $user = User::findIdentityByAccessToken($token);
        if($user){
            $request = Yii::$app->request;
            $request->setBodyParams(['user' => $user]);
            return true;
        }else{
            return false;
        }
    }
}