<?php

use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

class BearerAuthController extends ActiveController
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'bearerAuth' => [
                'class' => HttpBearerAuth::className()
            ]
        ]);
    }
}

?>