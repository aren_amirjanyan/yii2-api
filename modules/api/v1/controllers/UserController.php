<?php

namespace app\modules\api\v1\controllers;

use Firebase\JWT\JWT;
use Yii;
use app\models\User;
use yii\base\ErrorException;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($this->action->id == 'login' || $this->action->id == 'logout') {
                return Yii::$app->auth->check();
            } elseif ($this->action->id != 'auth') {
                return Yii::$app->api->check();
            }
            return true;
        } else {
            return false;
        }
    }

    public function actions()
    {
        $actions = parent::actions(); // TODO: Change the autogenerated stub

        unset($actions['index']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function actionAuth()
    {
        if (!Yii::$app->request->post('email') || !Yii::$app->request->post('password')) {
            return [
                'data' => [],
                'success' => false,
                'message' => 'All fields are required.'

            ];
        }
        $user = User::findOne(['email' => Yii::$app->request->post('email')]);

        if ($user) {
            if ($user->validatePassword(Yii::$app->request->post('password'))) {
                $message = 'The account not active yet! Please check your email.';
                if ($user->access_token) {
                    $user->auth_key = $user->getJWT();
                    $user->save();
                    $headers = Yii::$app->response->headers;
                    $headers->set('x-authorization-token', $user->auth_key);
                    $message = 'Authentication successfully completed';
                }

                return [
                    'data' => [],
                    'success' => true,
                    'message' => $message

                ];
            } else {
                return [
                    'data' => [],
                    'success' => false,
                    'message' => 'Authentication failed. Wrong email/password.'

                ];
            }

        } else {
            return [
                'data' => [],
                'success' => false,
                'message' => 'The account not exist!'
            ];
        }
    }

    public function actionLogin()
    {
        $user = Yii::$app->request->getBodyParam('user');

        if ($user)
            return ['success' => true, 'data' => $user, 'message' => 'You already logged in!'];
        else
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
    }

    public function actionLogout()
    {
        $user = Yii::$app->request->getBodyParam('user');

        if ($user) {

            $headers = Yii::$app->request->headers;
            $headers->remove('x-authorization-token');

            return ['success' => true, 'data' => $user, 'message' => 'You already log out in!'];
        } else
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
    }

    public function actionRegistration()
    {
        $user = new User();
        $user->first_name = Yii::$app->request->post('first_name');
        $user->last_name = Yii::$app->request->post('last_name');
        $user->username = Yii::$app->request->post('username');
        $user->password = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('password'));
        $user->email = Yii::$app->request->post('email');

        if ($user->save()) {

            $sendEmail = Yii::$app->mailer->compose(['jade' => '@app/modules/api/v1/email-verification'], [
                'to' => $user->email, // REQUIRED. This can be a comma delimited string just like a normal email to field.
                'subject' => 'Verification Email', // REQUIRED.
                'otherProperty' => [
                    'site_url' => Yii::$app->request->headers->get('origin'),
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'return_url' => Yii::$app->request->headers->get('origin') . '/activation/' . $user->id]])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject('Verification Email')
                ->send();

            if ($sendEmail) {
                $messages = 'The verification email sent, please check your email address.';
            } else {
                $messages = 'There was an error sending the email';
            }
            return ['success' => true, 'data' => [], 'message' => 'Successful created new user.' . $messages];

        } else {
            return ['success' => false, 'data' => [], 'message' => $user->getErrors()];
        }

    }

    public function actionActivate($id)
    {
        if (!$id) {
            return ['success' => false, 'data' => [], 'message' => 'Unknown user.'];
        }

        $user = User::findOne(['id' => $id]);

        if ($user) {

            $user->access_token = JWT::encode(['jti' => $user->id, 'iss' => $user->username, 'aud' => $user->email, 'iat' => time()], Yii::$app->params['secretKey']);

            if ($user->save()) {
                return ['success' => true, 'data' => [], 'message' => 'The user profile successfully activated!'];
            } else {
                return ['success' => false, 'data' => [], 'message' => $user->getErrors()];
            }

        } else
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
    }

    public function actionForgotPassword()
    {
        if (!Yii::$app->request->post('email')) {
            return ['success' => false, 'data' => [], 'message' => 'The Email fields is required.'];
        }
        $email = Yii::$app->request->post('email');

        $user = User::findOne(['email' => $email]);

        if ($user) {
            $sendEmail = Yii::$app->mailer->compose(['jade' => '@app/modules/api/v1/email-forgot'], [
                'to' => $user->email, // REQUIRED. This can be a comma delimited string just like a normal email to field.
                'subject' => 'Forgot Password', // REQUIRED.
                'otherProperty' => [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'return_url' => Yii::$app->request->headers->get('origin') . '/reset-password/' . $user->id]])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject('Forgot Password')
                ->send();

            if ($sendEmail) {
                return ['success' => true, 'data' => [], 'message' => 'Email Sent'];
            } else {
                return ['success' => false, 'data' => [], 'message' => 'There was an error sending the email'];
            }
        } else {
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
        }

    }

    public function actionResetPassword($id)
    {
        if (!Yii::$app->request->post('password')) {
            return ['success' => false, 'data' => [], 'message' => 'The Password fields is required.'];
        }
        if (!$id) {
            return ['success' => false, 'data' => [], 'message' => 'Unknown user.'];
        }

        $user = User::findOne(['id' => $id]);
        if ($user) {
            $user->password = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('password'));
            if ($user->save()) {
                $sendEmail = Yii::$app->mailer->compose(['jade' => '@app/modules/api/v1/email-reset'], [
                    'to' => $user->email, // REQUIRED. This can be a comma delimited string just like a normal email to field.
                    'subject' => 'Reset Password', // REQUIRED.
                    'otherProperty' => [
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'username' => $user->username,
                        'password' => Yii::$app->request->post('password')]])
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($user->email)
                    ->setSubject('Reset Password')
                    ->send();

                if ($sendEmail) {
                    $messages = 'Please check your email address for seeing new password.';
                } else {
                    $messages = 'There was an error sending the email.';
                }
                return ['success' => true, 'data' => [], 'message' => 'The password successfully updated!' . $messages];
            } else {
                return ['success' => false, 'data' => [], 'message' => $user->getErrors()];
            }
        } else {
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
        }
    }

    public function actionUpdate($id)
    {
        if (!$id) {
            return ['success' => false, 'data' => [], 'message' => 'Unknown user.'];
        }
        $user = User::findOne(['id' => $id]);
        if ($user) {
            $user->first_name = Yii::$app->request->post('first_name');
            $user->last_name = Yii::$app->request->post('last_name');
            $user->username = Yii::$app->request->post('username');
            $user->password = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('password'));
            $user->email = Yii::$app->request->post('email');
            if ($user->save()) {
                return ['success' => true, 'data' => [], 'message' => 'The profile successfully updated!'];
            } else {
                return ['success' => false, 'data' => [], 'message' => $user->getErrors()];
            }
        } else {
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
        }
    }

    public function actionDelete($id)
    {
        if (!$id) {
            return ['success' => false, 'data' => [], 'message' => 'Unknown user.'];
        }

        $user = User::findOne(['id' => $id]);

        if ($user) {
            if ($user->delete()) {
                return ['success' => true, 'data' => [], 'message' => 'The user profile successfully removed!'];
            } else {
                return ['success' => false, 'data' => [], 'message' => $user->getErrors()];
            }
        } else {
            return ['success' => false, 'data' => [], 'message' => 'User not found!'];
        }
    }

    public function actionAll()
    {
        $users = User::find()
            ->select('id,first_name, last_name,username, email')
            ->all();

        if ($users) {
            return ['success' => true, 'data' => $users, 'message' => 'The users list'];
        } else {
            return ['success' => false, 'data' => [], 'message' => 'Users not found!'];
        }
    }

    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'auth' => ['POST'],
            'login' => ['POST'],
            'logout' => ['GET', 'HEAD'],
            'registration' => ['POST'],
            'activate' => ['PUT', 'PATCH'],
            'forgotPassword' => ['POST'],
            'resetPassword' => ['PUT', 'PATCH'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'all' => ['GET', 'HEAD'],
        ]);
    }

}
