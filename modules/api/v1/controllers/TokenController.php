<?php

namespace app\modules\api\v1\controllers;

use Firebase\JWT\JWT;
use Yii;
use yii\rest\ActiveController;

use app\modules\api\v1\models\Token;

class TokenController extends ActiveController
{
    public $modelClass = 'app\modules\api\v1\models\Token';

    public function actionGetToken()
    {

        if(!Yii::$app->request->post('app_id') && !Yii::$app->request->get('app_id')){
            return [
                'data' => [],
                'success' => false,
                'message' => 'The application ID required!'
            ];
        }

        $application_id = (Yii::$app->request->post('app_id'))? Yii::$app->request->post('app_id') : Yii::$app->request->get('app_id');

        $token = Token::findOne(['app_id' => $application_id]);
        if($token){
            $newToken = JWT::encode(['jti'=>$token->app_id,'aud'=> $token->name,'iat'=>time()],Yii::$app->params['secretKey']);
            $token->secret = $newToken;
            if($token->save()){

                $headers = Yii::$app->response->headers;
                $headers->set('x-api-token', $token->secret);

                return [
                    'data' => [],
                    'success' => false,
                    'message' => 'The Application token have updated!'
                ];
            }else{
                return [
                    'data' => [],
                    'success' => false,
                    'message' => 'Can not update token in application'
                ];
            }
        }else{
            return [
                'data' => [],
                'success' => false,
                'message' => 'The application not found!'
            ];
        }
    }
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'getToken' => ['GET', 'HEAD','POST'],
        ]);
    }

}
