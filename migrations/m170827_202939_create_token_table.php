<?php

use yii\db\Migration;

/**
 * Handles the creation of table `token`.
 */
class m170827_202939_create_token_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('token', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(255)->unique()->notNull(),
            'app_id'=>$this->string(255)->unique()->notNull(),
            'secret'=>$this->string(255)->unique()->notNull(),
            'created_at'=>$this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('token');
    }
}
