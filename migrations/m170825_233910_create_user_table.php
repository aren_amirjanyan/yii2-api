<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170825_233910_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'first_name'=>$this->string(255)->notNull(),
            'last_name'=>$this->string(255)->notNull(),
            'username'=>$this->string(255)->unique()->notNull(),
            'password'=>$this->string(255)->notNull(),
            'email'=>$this->string(255)->unique()->notNull(),
            'auth_key'=>$this->string(255)->notNull(),
            'access_token'=> $this->string(255)->notNull()

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
