# Yii2 REST API #

The project is REST API with Yii2, MySQL 

### How run REST API ? ###

* Clone repository - [git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/yii2-api.git](git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/yii2-api.git)
* composer install
* check files of /config folder and change with your credentials
* php yii migrate

### Documentation of REST API
[https://bitbucket.org/aren_amirjanyan/nodejs-mongo-api/wiki/Home](https://bitbucket.org/aren_amirjanyan/nodejs-mongo-api/wiki/Home)